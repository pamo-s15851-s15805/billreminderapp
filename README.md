# Temat projektu: BillsReminderApp

# Skład zespołu projektowego:
- Michał Borkowski, s15805
- Kamil Szczepanik, s15851

# Środowisko wytwórcze:
- narzędzie programistyczne: Android Studio 
- język: Java


# Zakres projektu:
- implementacja oraz testowanie aplikacji
- stworzenie dokumentacji kodu źródłowego
- publikacja postępów prac nad projektem (Trello)
- wprowadzenie przykładowych danych do bazy

# Cel projektu:
Celem projektu jest wytworzniie aplikacji mobilnej dedykowanej wszystkim płatnikom, która będzie w sposób intuicyjny i niezawodny zarządzała harmonogramem 
płatności oraz będzie przypominała o płatnościach rachunków. BillsReminderApp ma umożliwić stworzenie bazy zarówno cyklicznych jak i niespodziewanych 
płatności, w znaczny sposób ułatwić przeszukiwanie tej bazy w poszukiwaniu nadchodzących, zaległych oraz dokonanych płatności, co ma ułatwić użytkownikowi 
dotrzymanie terminów płatności.

# Najważniejsze funkcjonalności użytkowe aplikacji:
- dodawanie pozycji nadchodzących płatności z określeniem jej nazwy, terminu płatności, kwoty zobowiązania, numeru konta, oznaczeniu sposobu przypominania
  o płatnościach czy kategoryzowanie zobowiązań
- możliwość wyszukania płatności nieopłaconych, zaległych i nadchodzących.



